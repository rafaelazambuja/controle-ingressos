package br.com.venda.ingresso.dominio;

public class Venda {
    
    public static final int OPCAO_PAGAMENTO_DINHEIRO = 1;
    public static final int OPCAO_PAGAMENTO_DEBITO = 2;
    public static final int OPCAO_PAGAMENTO_CREDITO = 3;
    
    private Sessao sessao;
    private Assento assento;
    private int opcaoPagamento;
    private int numero;

    public Sessao getSessao() {
        return sessao;
    }

    public void setSessao(Sessao sessao) {
        this.sessao = sessao;
    }

    public Assento getAssento() {
        return assento;
    }

    public void setAssento(Assento assento) {
        this.assento = assento;
    }

    public int getOpcaoPagamento() {
        return opcaoPagamento;
    }

    public void setOpcaoPagamento(int opcaoPagamento) {
        this.opcaoPagamento = opcaoPagamento;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
    
}
