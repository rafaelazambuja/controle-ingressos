package br.com.venda.ingresso.dominio;


public class Filme {
    
    private String nome;
    private String duracao;
    private String sinopse;
    

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDuracao() {
        return duracao;
    }

    public void setDuracao(String duracao) {
        this.duracao = duracao;
    }
    
    
}
