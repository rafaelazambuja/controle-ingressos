package br.com.venda.ingresso.dominio;

import java.util.Date;

public class Sessao {
    
    private int sala;
    private Filme filme;
    private Date horario;

    public int getSala() {
        return sala;
    }

    public void setSala(int sala) {
        this.sala = sala;
    }

    public Filme getFilme() {
        return filme;
    }

    public void setFilme(Filme filme) {
        this.filme = filme;
    }

    public Date getHorario() {
        return horario;
    }

    public void setHorario(Date horario) {
        this.horario = horario;
    }
    
    
}
